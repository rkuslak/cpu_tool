# cpu\_tool
A simple program to show and update the scaling governors on CPUs in Linux.

# USAGE
To see all available options, run `cpu_tool -h`. This will show the currently
available options. In general, to set a governor pass the name of the governor
with the `--governor` option, or the name of the energy profile with `--energy`.
To see the current settings, run the program without options or include
`--verbose` to print them after program execution.

```
FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v               Turns on verbose output of actions taken

OPTIONS:
    -g, --governor <GOVERNOR>         Attempts to set all profiles governors to the given GOVERNOR
    -p, --profile <ENERGY_PROFILE>    Attempts to set all profiles energy profile to the given ENERGY_PROFILE
```

## TODO
* Additional clarification in README
* Provide DBUS server to no longer need to pass this superuser permissions
