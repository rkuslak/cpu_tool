/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

extern crate ansi_term;
#[macro_use]
extern crate clap;
extern crate failure;

mod command_line;
mod profile;

use std::fmt::Display;

fn display_error_msg<T>(msg: T)
where
    T: Display,
{
    println!("{}", msg);
}

fn main() {
    let options = command_line::parse_cmd_line();
    let mut profiles = profile::get_profiles().expect("Failed to pull profiles");

    if let Some(new_governor) = &options.new_governor {
        if options.verbose {
            println!("Attempting to set governor to {}", &new_governor);
        }
        for profile in profiles.iter_mut() {
            let result = profile.set_governor(&new_governor.clone());
            if result.is_err() {
                display_error_msg(format!("Failed to set governor \"{}\"", new_governor));
                return;
            }
        }
    }

    if let Some(new_profile) = &options.new_profile {
        if options.verbose {
            println!("Attempting to set profiles to {}", &new_profile);
        }
        for profile in profiles.iter_mut() {
            let result = profile.set_profile(&new_profile.clone());
            if result.is_err() {
                display_error_msg(format!("Failed to set profile \"{}\"", new_profile));
                return;
            }
        }
    }

    if (options.new_profile.is_none() && options.new_governor.is_none()) || options.verbose {
        profile::print_profiles(&profiles);
    }
}
