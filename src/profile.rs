/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use failure;
use failure::Error;

use ansi_term;
use ansi_term::Colour::{Cyan, Green, White};

use std::cmp::Ordering;
use std::fs;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::string::ToString;

const ROOT_PROFILE_PATH: &'static str = "/sys/devices/system/cpu/cpufreq/";
const GOVERNORS_AVAILABLE_FILE: &'static str = "scaling_available_governors";
const GOVERNORS_CURRENT_FILE: &'static str = "scaling_governor";
const ENERGY_PROFILES_CURRENT_FILE: &'static str = "energy_performance_preference";
const ENERGY_PROFILES_AVAILABLE_FILE: &'static str = "energy_performance_available_preferences";

#[derive(Clone, Debug, Eq)]
pub struct Profile {
    pub name: String,
    profile_directory: PathBuf,

    pub current_governor: String,
    pub available_governors: Vec<String>,

    pub current_energy_profile: String,
    pub available_energy_profile: Vec<String>,
}

impl Ord for Profile {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialOrd for Profile {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Profile {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Profile {
    /// Create a Profile based on the PathBuf pointing to a profile's root
    /// directory. Panics on failure
    pub fn from_pathbuf(profile_path: PathBuf) -> Profile {
        let mut infile_path = profile_path.clone();

        infile_path.push(GOVERNORS_CURRENT_FILE);
        let current_governor = fs::read_to_string(&infile_path)
            .expect("Failed to read governor file")
            .trim()
            .to_owned();

        infile_path.set_file_name(ENERGY_PROFILES_CURRENT_FILE);
        let current_energy_profile = fs::read_to_string(&infile_path)
            .expect("Failed to read energy profiles file")
            .trim()
            .to_owned();

        infile_path.set_file_name(ENERGY_PROFILES_AVAILABLE_FILE);
        let available_energy_profile = pathbuf_to_vec(&infile_path);

        infile_path.set_file_name(GOVERNORS_AVAILABLE_FILE);
        let available_governors = pathbuf_to_vec(&infile_path);

        Profile {
            name: profile_path
                .file_name()
                .expect("Wut?")
                .to_string_lossy()
                .to_string(),
            profile_directory: profile_path,

            current_governor,
            current_energy_profile,

            available_governors,
            available_energy_profile,
        }
    }

    /// Attempts to set the current energy profile for this profile to the passed string. Returns
    /// Ok(()) on success, or a failure::Error wrapping the returned error on failure.
    pub fn set_profile(&mut self, new_profile: &String) -> Result<(), Error> {
        if !self
            .available_energy_profile
            .contains(&new_profile.to_lowercase())
        {
            let msg = format!("Provided energy profile not available: {}", new_profile);
            return Err(failure::err_msg(msg));
        }

        let mut profile_path = self.profile_directory.clone();
        profile_path.push(ENERGY_PROFILES_CURRENT_FILE);
        self.current_energy_profile = Profile::update_file(profile_path, new_profile)?;

        Ok(())
    }

    /// Attempts to set the current power governor for this profile to the passed string. Returns
    /// Ok(()) on success, or a failure::Error wrapping the returned error on failure.
    pub fn set_governor(&mut self, new_governor: &String) -> Result<(), Error> {
        if !self
            .available_governors
            .contains(&new_governor.to_lowercase())
        {
            let msg = format!("Provided governor not available: {}", new_governor);
            return Err(failure::err_msg(msg));
        }

        let mut profile_path = self.profile_directory.clone();
        profile_path.push(GOVERNORS_CURRENT_FILE);
        self.current_governor = Profile::update_file(profile_path, new_governor)?;

        Ok(())
    }

    fn update_file(profile_path: PathBuf, new_content: &String) -> Result<String, Error> {
        let mut outfile = OpenOptions::new()
            .write(true)
            .append(false)
            .open(&profile_path)?;
        outfile.write_all(new_content.as_bytes())?;
        outfile.sync_all()?;

        let infile = fs::read_to_string(profile_path)?.trim().to_owned();
        Ok(infile)
    }
}

/// Converts the given Pathbuf filepath into a vector of whitespace-delimited,
/// trimmed Strings. If we are unable to read the file, returns a empty Vec.
fn pathbuf_to_vec(filename: &PathBuf) -> Vec<String> {
    let mut results: Vec<String> = Vec::new();

    if let Ok(file_data) = fs::read_to_string(filename) {
        let splits = file_data.split_whitespace();
        for result in splits {
            results.push(result.trim().to_owned());
        }
    }

    results
}

fn combined_path_exists(root_path: &PathBuf, filename: &str) -> bool {
    let mut full_path = root_path.clone();
    full_path.push(filename);

    full_path.exists()
}

// TODO: Should we retain non-union members of the disparate profiles that are dropped as available
// options and log/show them somehow? Appears as if these should always be the same for each
// profile, but can we assume that?
pub fn print_profiles(profiles: &[Profile]) {
    const NAME_WIDTH: usize = 20;
    const CURRENT_WIDTH: usize = 30;
    const AVAILABLE_WIDTH: usize = 50;

    let display_width: usize = NAME_WIDTH + (2 * CURRENT_WIDTH) + 2;
    let header_color = Cyan.bold();
    let name_color = Green.bold();
    let value_color = Green;
    let (header_prefix, header_suffix) = (header_color.prefix(), header_color.suffix());
    let (name_prefix, name_suffix) = (name_color.prefix(), name_color.suffix());
    let (value_prefix, value_suffix) = (value_color.prefix(), value_color.suffix());

    println!(
        "{}{:name_width$} {:current_width$} {:current_width$}{}",
        header_prefix,
        "PROFILE",
        "GOVERNOR",
        "ENERGY PROFILE",
        header_suffix,
        name_width = &NAME_WIDTH,
        current_width = &CURRENT_WIDTH,
    );

    for _ in 0..display_width {
        print!("=");
    }
    println!();

    let mut union_governors = profiles[0].available_governors.clone();
    let mut union_energy_profiles = profiles[0].available_energy_profile.clone();

    for profile in profiles.iter() {
        union_governors.retain(|p| profile.available_governors.contains(p));
        union_energy_profiles.retain(|p| profile.available_energy_profile.contains(p));

        let mut current_governor = profile.current_governor.clone();
        let mut current_energy_profile = profile.current_energy_profile.clone();
        current_governor.truncate(CURRENT_WIDTH.clone());
        current_energy_profile.truncate(CURRENT_WIDTH.clone());

        let mut available_governors = profile.available_governors.join(", ");
        let mut available_energy_profile = profile.available_energy_profile.join(", ");
        available_governors.truncate(AVAILABLE_WIDTH.clone());
        available_energy_profile.truncate(AVAILABLE_WIDTH.clone());

        println!(
            // "{:name_width$} {:current_width$} {:current_width$}",
            "{}{:name_width$}{} {}{:current_width$} {:current_width$}{}",
            name_prefix,
            profile.name,
            name_suffix,
            value_prefix,
            current_governor,
            current_energy_profile,
            value_suffix,
            name_width = &NAME_WIDTH,
            current_width = &CURRENT_WIDTH,
        )
    }

    println!(
        "\n{}: {}\n{}: {}",
        Green.bold().paint("AVAILABLE GOVERNORS").to_string(),
        union_governors
            .iter()
            .map(|p| White.bold().paint(p).to_string())
            .collect::<Vec<String>>()
            .join(", "),
        Green.bold().paint("AVAILABLE ENERGY PROFILES").to_string(),
        union_energy_profiles
            .iter()
            .map(|p| White.bold().paint(p).to_string())
            .collect::<Vec<String>>()
            .join(", ")
    );
}

pub fn get_profiles() -> Result<Vec<Profile>, &'static str> {
    let mut results: Vec<Profile> = Vec::new();

    let profiles_dir = Path::new(ROOT_PROFILE_PATH);

    match profiles_dir.read_dir() {
        Ok(potential_profiles) => {
            for profile_path in potential_profiles
                .filter_map(|x| x.ok())
                .map(|dir_entry| dir_entry.path())
                .filter(|path| combined_path_exists(&path, GOVERNORS_AVAILABLE_FILE))
            {
                results.push(Profile::from_pathbuf(profile_path));
            }
            results.sort();
            Ok(results)
        }
        _ => Err("Failed to open profiles directory"),
    }
}
