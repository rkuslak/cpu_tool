/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use clap;
use clap::{App, Arg};

const VERBOSE_STRING: &'static str = &"verbose";
const GOVERNOR_STRING: &'static str = &"governor";
const PROFILE_STRING: &'static str = &"profile";

pub struct Options {
    pub verbose: bool,
    pub new_profile: Option<String>,
    pub new_governor: Option<String>,
}

pub fn parse_cmd_line() -> Options {
    let mut app = App::new(crate_name!())
        .version(crate_version!())
        .about("A simple application to control CPU power profiles under linux")
        .author(crate_authors!());

    let args: Vec<Arg> = vec![
        Arg::with_name(VERBOSE_STRING)
            .short("v")
            .help("Turns on verbose output of actions taken"),
        Arg::with_name(GOVERNOR_STRING)
            .short("g")
            .long(GOVERNOR_STRING)
            .help("Attempts to set all profiles governors to the given GOVERNOR")
            .value_name(GOVERNOR_STRING),
        Arg::with_name(PROFILE_STRING)
            .short("p")
            .long(PROFILE_STRING)
            .help("Attempts to set all profiles energy profile to the given ENERGY_PROFILE")
            .value_name(PROFILE_STRING),
    ];

    for arg in args.iter() {
        app = app.arg(arg);
    }

    let matches = app.get_matches();
    let mut options = Options {
        verbose: matches.is_present(VERBOSE_STRING),
        new_profile: None,
        new_governor: None,
    };

    if let Some(new_profile) = matches.value_of(PROFILE_STRING) {
        options.new_profile = Some(new_profile.to_owned());
    }
    if let Some(new_governor) = matches.value_of(GOVERNOR_STRING) {
        options.new_governor = Some(new_governor.to_owned());
    }

    options
}
